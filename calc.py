# calc: a stupidly simple REPL calculator
# Version: epsilon > 0
# Author: LucasVB
# License: Public Domain
# 
# NOTES:
# Use at your own risk.
# This program was developed because I always felt the need for an ultra-lightweight calculator
#     with no interface that took straight up mathematical expressions and printed a result.
# Simple as that.
# I wanted something I can run on a keyboard shortcut, open instantly, type some math, hit enter
#     and get a result.
# I'm not interested in expanding this into a full fledged application.
# (But I *might* write a nice proper multiplatform version later, who knows?)  
# This was tested on Windows, Python 2.7
# I'm not gonna worry about other systems right now...
# Niceties of the console window were implemented via the properties in the .lnk file

from __future__ import division # always use / as float division, as in Python 3

import os
import sys
import re
import types
import pyperclip
from math import *
from os import system

def __clear_console():
    if os.name in ('ce', 'nt'):
        system('cls')
    elif os.name == 'posix':
        system('clear')
    else:
        print('\n' * 120)
        
__clear_console() # clear console junk immediately

# Internal variables
__customdefs = list() # variables/functions defined in the session
__contents = list() # __contents of the session (ignoring commands)
__lastfn = "" # last filename saved to
__scidigits = 0 # scientific notation, decimal digits... if zero, show numbers as is
__lastresult = None

# Signature for functions
# There's a better way to detect identifiers than this, but functions shouldn't use
# special characters
__FUNCTION_SIGNATURE = '^([a-zA-Z0-9_]+)\((.+?)\)$'

# Define some extra functions and constants
tau = 2*pi
def ln(x): return log(x)
def sec(x): return 1.0/cos(x)
def csc(x): return 1.0/sin(x)
def cot(x): return 1.0/tan(x)

# Keep at it until we quit
while True:
    try:
        sys.stdout.write('> ')
        
        __input = raw_input()
        __input = re.sub("\s+", " ", __input.strip()) # cleanup extra spaces
        
        # Replace @ with the last result
        # This helps if we want x = @, better than typing anything again
        __input = __input.replace("@", str(__lastresult) if __lastresult is not None else "")
        
        # If blank, just run again
        if not __input:
            continue;
            
        if __input == "exit" or __input == "quit":
            sys.exit()
        
        __firstchr = __input[0] # get first character
        
        # Typing just ` or ' inverts (1/x)
        if __firstchr in "'`": 
            if __lastresult is not None:
                __input = "1/"+str(__lastresult);
            else:
                print("ERROR: nothing to invert")
                continue
        
        # Special commands. These are not logged.
        if __firstchr == ":": # we're running a special command
            __tokens = __input.split(" ") # get arguments
            __cmd = __tokens[0][1:].lower()
            
            # Copy last known result to clipboard
            if __cmd == "x":
                if __lastresult is not None:
                    pyperclip.copy(str(__lastresult))
                continue
                
            # Copy last known result to clipboard and quit
            if __cmd == "xq":
                if __lastresult is not None:
                    pyperclip.copy(str(__lastresult))
                sys.exit()
            
            # Scientific notation mode
            if __cmd == "sci" or __cmd == "d": 
                if len(__tokens) >= 2:
                    try:
                        __scidigits = int(__input.split(" ")[1].strip())
                    except Exception:
                        __scidigits = -1
                    if __scidigits < 0:
                        print("ERROR: invalid number for significant figures")
                        continue
                else:
                    __scidigits = 0
                if __lastresult is not None:
                   __input = str(__lastresult)
                else:
                    continue
            
            
            # Clear window and log
            # Keeps variables and functions
            if __cmd == "clear" or __cmd == "c":
                __clear_console()
                __contents = list()
                __lastfn = ""
                continue
                
                
            # Clear window, log and all declared functions and variables
            if __cmd == "new" or __cmd == "n":
                __clear_console()
                __lastresult = None
                __contents = list()
                __lastfn = ""
                for v in __customdefs:
                    if v in vars():
                        del vars()[v]
                continue
        
            # Exit
            if __cmd == "exit" or __cmd == "quit" or __cmd == "q":
                sys.exit()
        
            # delete variable/function statement
            if __cmd == "del":
                exec(__input[1:])
                continue
             
            # Save to file, no need for quotes in filename
            # After a file is saved, you can just call save again to save to the same file
            if __cmd == "save":
                __fn = " ".join(__tokens[1:])
                if __fn:
                    __lastfn = __fn
                else:
                    if __lastfn:
                        __fn = __lastfn
                    else:
                        print('ERROR: no filename given')
                        continue
                try:
                    with open(__fn, "w") as __f:
                        __f.write("\n".join(__contents))
                    print(': Saved to "%s"' % __fn)
                except Exception:
                   print(": Couldn't save")
                continue
        
        
        # Otherwise, we're running an expression
        __contents.append("> " + __input)
        
        # Detect factorials in the standard notation and replaces them with factorial()
        # This algorithm can be done with regex, but it looks terrible!
        while "!" in __input:
            __fend = __input.find("!") # location of the current factorial being processed
            __fstart = __fend # start of the string block that goes into the factorial
            __np = 0 # number of parenthesis
            while __fstart >= 0: # keep going back until beginning of expression 
                __fstart -= 1 # move back beginning of block
                __c = __input[__fstart:__fstart+1] # get the character there
                if __c == ")": # if we're closing a parenthesis block
                    __np += 1 # we count the number of parenthesis up, so we wait until it closes
                elif __c == "(": # if it's opening a parenthesis block
                    if __np == 0: # and we're not expecting more of an expression
                        break # end here, we have our factorial block
                    __np -= 1 # otherwise, we close one of the parenthesis we're expecting
                elif __c in " +-*/%^!": # if an operator or space
                    if __np > 0: # we keep going if we're still in a parenthesis block
                        continue
                    else: # otherwise, we found our block to go inside the factorial
                        break
            __fstart += 1 # move the start one character right, since we overshot
            __input = __input.replace(
                __input[__fstart:__fend+1],"factorial(%s)" % __input[__fstart:__fend])
        
        # Exponentiation
        __input = __input.replace("^","**") # make sure to use Python's horrible ** notation for powers
        
        # If it's a declaration, but not a boolean condition
        if  "=" in __input and \
            "==" not in __input and \
            "<=" not in __input and \
            ">=" not in __input:
            __lhs = __input.split("=")[0].strip()
            __rhs = __input.split("=")[1].strip()
            if re.match(__FUNCTION_SIGNATURE, __lhs): # if it's a function
                __input = "def %s: return (%s)" % (__lhs, __rhs) # define as a function
                # add function name to the list of custom definitions
                __customdefs.append(re.search(__FUNCTION_SIGNATURE, __lhs).group(1))
                exec(__input)
            else: # otherwise, a variable declaration and we just straight up exec() it
                __customdefs.append(__lhs)
                exec(__input)
        else: # if not, it's just an expression, just evaluate it then
            __res = eval(__input)
            # if it's an integer, and not a boolean, make sure to show it as an int
            if not type(__res) == types.BooleanType:
                try:
                    if __res == int(__res):
                        __res = int(__res)
                except TypeError:
                    print("ERROR: invalid expression")
                    continue
            __lastresult = __res # keep track of last known result
            if __scidigits == 0: # if not using scientific notation
                print("= " + str(__res)) # write to console normally
                __contents.append("= " + str(__res)) # write to log
            else: # we format accordingly
                # gotta create the formatting string first
                __fmt = "= %."+str(__scidigits-1)+"e"; 
                print(__fmt % float(__res))
                __contents.append("= " + __fmt % float(__res)) # write to log

                
    # Detect errors. Kind of a hack, not comprehensive.
    # It just has to handle the proper math and definition errors.
    except KeyboardInterrupt as exception: # Bugs out sometimes. No idea why... yet.
        sys.exit()
    except EOFError as exception:
        print('ERROR: invalid expression')
    except SyntaxError as exception:
        print('ERROR: invalid expression')
    except NameError as exception:
        print('ERROR: %s' % str(exception))
    except ValueError as exception:
        print('ERROR: %s' % str(exception))
    except TypeError as exception:
        print('ERROR: %s' % str(exception))
    except OverflowError as exception:
        print('ERROR: resulting value is too large')
    except ZeroDivisionError as exception:
        print('ERROR: %s' % str(exception))
    except Exception as exception: # Anything else I'll figure it out later.
        print('ERROR: %s' % repr(exception))
        # sys.exit()

# end of file